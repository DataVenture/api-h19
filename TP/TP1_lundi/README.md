# TP1 Lundi 21/01/2019

Aujourd'hui vous allez prendre en mains Python et quelques unes des libraires:

* Numpy
* Matplotlib
* Scikit-Learn

Commencez par le notebook `python-numpy-matplotlib.ipynb` (~1h pas plus) puis les notebooks dans le dossier `machine learning` en commençant par `05.00-Machine-Learning.ipynb`.

Vous aurez sans doute à installer le package `seaborn` dans anaconda.