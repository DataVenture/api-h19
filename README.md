# La Data Science pour tous

![](dataventure.png)

## Liens utiles

Moodle: [https://moodle.utc.fr/course/view.php?id=2291](https://moodle.utc.fr/course/view.php?id=2291)

Slack: [https://api-dataventure-h19.slack.com/](https://api-dataventure-h19.slack.com/)

Hackmd: [https://hackmd.io/bpc6e_tCTcevegvqyUDXuQ](https://hackmd.io/bpc6e_tCTcevegvqyUDXuQ)

## Contact 
Une question ? Consultez la FAQ en pdf sur Moodle. Sinon faites nous un mail à [dataventure@assos.utc.fr](mailto:dataventure@assos.utc.fr)

## Droits d'auteur

La présentation python est placée sous licence [CC BY][ccby]

[CC BY]: https://creativecommons.org/licenses/by/4.0/
